#ifndef RENDERER_H
#define RENDERER_H

#include "dataManager/dataManager.hpp"
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
// #include <SOIL.h>
#include <glm/glm.hpp>
// #include <glm/gtc/matrix_transform.hpp>
// #include <glm/gtc/type_ptr.hpp>

class Renderer
{
  private:
    GLuint sceneVertexShader;
    GLuint sceneFragmentShader;
    GLuint sceneShaderProgram;

    GLuint vaoPoints;
    GLuint vboPoints;

    DataManager data;

  public:
    Renderer( );
    ~Renderer( );
    void draw( float& time );
};

#endif