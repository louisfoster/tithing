#include "dataManager.hpp"

DataManager::DataManager( )
{
    redisContext* c = redisConnect( "127.0.0.1", 6379 );
    if ( c == NULL || c->err )
    {
        if ( c )
        {
            printf( "Error: %s\n", c->errstr );
            // handle error
        }
        else
        {
            printf( "Can't allocate redis context\n" );
        }
        return;
    }

    context = c;
    users   = 0;
    size    = NULL;
    buff    = NULL;
}

DataManager::~DataManager( )
{
    freeData( );
    redisFree( context );
}

void DataManager::freeData( )
{
    if ( users > 0 )
    {
        for ( int i = 0; i < users; ++i )
        {
            delete[] buff[ i ];
        }

        delete[] size;
        delete[] buff;

        users = 0;
    }
}

void DataManager::update( )
{
    freeData( );
    void* pointer = NULL;

    redisReply* usersReply;
    pointer    = redisCommand( context, "GET users" );
    usersReply = (redisReply*)pointer;

    if ( usersReply->type == REDIS_REPLY_ERROR )
    {
        printf( "Error: %s\n", usersReply->str );
        freeReplyObject( usersReply );
        return; // Throw?
    }
    else if ( usersReply->type == REDIS_REPLY_NIL ||
              usersReply->type != REDIS_REPLY_STRING )
    {
        freeReplyObject( usersReply );
        return; // Throw?
    }

    users = atoi( usersReply->str );
    freeReplyObject( usersReply );
    buff = new float*[ users ];
    size = new int[ users ];

    for ( int i = 0; i < users; i++ )
    {
        redisReply* reply;
        char buffer[ 50 ];
        sprintf( buffer, "LRANGE %d 0 -1", i );
        pointer = redisCommand( context, buffer );
        reply   = (redisReply*)pointer;

        if ( reply->type == REDIS_REPLY_ERROR )
        {
            printf( "Error: %s\n", reply->str );
        }
        else if ( reply->type != REDIS_REPLY_ARRAY )
        {
            printf( "Unexpected type: %d\n", reply->type );
        }
        else
        {
            buff[ i ] = new float[ reply->elements ];
            for ( int j = 0; j < reply->elements; j++ )
            {
                float x = strtof( reply->element[ j ]->str, NULL );

                buff[ i ][ j ] = x;
            }
            size[ i ] = reply->elements;
        }

        freeReplyObject( reply );
    }
}
