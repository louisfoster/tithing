#include "context/context.hpp"
#include "context/contextException.hpp"
#include "renderer/renderer.hpp"
// #include <SDL2/SDL.h>
// #include <SDL2/SDL_opengl.h>

int main( int, char* [] )
{
    try
    {
        Context context;
        Renderer r;

        if ( !context.eventLoop( r ) )
        {
            return EXIT_SUCCESS;
        }
    }
    catch ( ContextException& e )
    {
        std::cout << "Context exception caught." << std::endl;
        std::cout << e.message( ) << std::endl;

        return EXIT_FAILURE;
    }
}