#ifndef CONTEXT_EXCEPTION_H
#define CONTEXT_EXCEPTION_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <exception>

enum ContextExceptionType
{
    noSDL,
    noWindow
};

struct ContextException : public std::exception
{
    ContextExceptionType type;

    ContextException( ContextExceptionType _type )
    {
        type = _type;
    }

    const char* message( ) const throw( )
    {
        char buffer[ 100 ];

        switch ( type )
        {
        case noSDL:
            strcpy( buffer, "SDL could not initialize! SDL Error: " );
            strcat( buffer, SDL_GetError( ) );
            break;
        case noWindow:
            strcpy( buffer, "Window not created: " );
            strcat( buffer, SDL_GetError( ) );
            break;
        };

        return buffer;
    }
};

#endif