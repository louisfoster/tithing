#ifndef CONTEXT_H
#define CONTEXT_H

#include "../renderer/renderer.hpp"
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <iostream>

class Context
{
  private:
    SDL_Window* window;
    SDL_GLContext context;
    void checkGLErrors( );

  public:
    Context( );
    ~Context( );
    void close( );
    bool eventLoop( Renderer& );
};

#endif