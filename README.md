# tithing

## Setup

- install nginx, redis, go
- add `127.0.0.1 tithing.live` to `/etc/hosts`
- symlink path to nginx conf file with correct path from nginx's serve dir
- install go deps
- activate nginx and redis servers
- from the `web/server` directory, run: `go run *.go`
- open browser to `tithing.live`

## TODO

- send timestamp of input event -> websocket send event -> server receive event -> db insert event -> db first retreive event
- script to restart everything

- comments
- docs
- test

### Web

- script setup for nginx install and symlinking + paths etc.
- restart nginx on change of config file
- possibly change this system to use [go webrtc](https://github.com/pions/webrtc)
- send data as bin not json
- setup `/etc/hosts` with appropriate URL

### Visuals

- get framerate
- first render not occuring until interaction/focus on window (ie moving the window)?
- use interfaces/classes and better structure
